package br.com.aluno.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe para maninuplar conexões com banco de dados (MySQL)
 * 
 * @author Wellington 02/02/2022
 * @version 1.0
 * @since 1.0
 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/Connection.html
 * 
 */

public class ConnectionFactory {
	
	private static Connection connection;
		
		/**
		 * Método para estabelecer uma conexão com a base de dados
		 * 
		 * @author Wellington 02/02/2022
		 * @version 1.0
		 * @since 1.0
		 * @throws Exception
		 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html
		 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
		 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/ClassNotFoundException.html
		 * @return retorna uma conexão com o banco de dados
		 * 
		 */
	
	   public static Connection getConnection() throws Exception {
			
			connection = null; // inicializa a variável de conexão nula
			
			try {
					
				   Class.forName("com.mysql.cj.jdbc.Driver"); // Configuração do driver
					connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/aluno?useTimezone=true&serverTimezone=UTC"
							+ "&autoReconnect=true&useSSL=false", "root", "well123"); // inicializar a conexão com a base de dados
								
			}catch(ClassNotFoundException cfe) {
				
				System.out.println("Driver não encontrado: "+cfe.getMessage()); // Imprime a mensagem do erro (Console)
							
			}catch(SQLException sqle) {
				
				System.out.println("SQLException: " + sqle.getMessage()); // Imprime a mensagem do erro (Console)
			    System.out.println("SQLState: " + sqle.getSQLState()); // Imprime o estado do erro (Console)
			    System.out.println("Erro: " + sqle.getErrorCode()); // Imprime o código do erro (Console)
			    System.out.println("StackTrace: " + sqle.getStackTrace()); // Imprime o rastreamento do erro	 (Console) 
				
			} catch (Exception ex) {
				
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
				
			}
			
			return connection;
		}
	   
	   // ------------------------------------------------------------------------------------------------------------
	   
	   /**
		 * Método auxiliar para encerrar uma conexão com banco de dados
		 * 
		 * @author Wellington 02/02/2022
		 * @version 1.0
		 * @since 1.0
		 * @param conn objeto de conexão
		 * @param ps objeto de declaração da query
		 * @param rs objeto resultado de consulta
		 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html		
		 * 
		 */
	
	   public static void closeConnection(Connection conn, PreparedStatement ps, ResultSet rs){
			
			try {
				
				close(conn, ps, rs);
		
			}catch(Exception ex) {
			
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
			}		
	   }
	   
	   // ------------------------------------------------------------------------------------------------------------

	   /**
		 * Método auxiliar para encerrar uma conexão com banco de dados
		 * 
		 * @author Wellington 02/02/2022
		 * @version 1.0
		 * @since 1.0	
		 * @param conn objeto de conexão
		 * @param ps objeto de declaração da query
		 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html		
		 * 
		 */
		   
		public static void closeConnection(Connection conn, PreparedStatement ps) {
			
			try {
			
				close(conn, ps, null); 
		
			}catch(Exception ex) {
				
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
			
			}		
	  }
		
	  // ------------------------------------------------------------------------------------------------------------

		/**
		 * Método para encerrar uma conexão com banco de dados
		 * Pode ser condicional se houver variáveis de uso comum
		 * 
		 * @author Wellington 02/02/2022
		 * @version 1.0
		 * @since 1.0
		 * @throws Exception
		 * @param conn objeto de conexão
		 * @param ps objeto de declaração da query
		 * @param rs objeto resultado de consulta
		 * @throws Exception
		 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
		 */
			
		private static void close(Connection conn, PreparedStatement ps, ResultSet rs) throws Exception {
			
				try {
					
					if (rs != null) // caso seja diferente de null (encerra)
							rs.close();
					
					if (ps != null) // caso seja diferente de null (encerra)
							ps.close();
					
					if (conn != null) // caso seja diferente de null (encerra)
							conn.close();
					
				} catch (Exception ex) {
					
					ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
					
				}
			
		}		
		 
	 // ------------------------------------------------------------------------------------------------------------

}
