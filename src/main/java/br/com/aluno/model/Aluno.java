package br.com.aluno.model;

/**
 * Classe para o modelo do Aluno
 * Construtores, Getters e Setters
 * 
 * @author Wellington 02/02/2022
 * @version 1.0
 * @since 1.0
 *
 */

public class Aluno {
	
	// Atributos
	
	private int id;
	private String matricula;
	private String nome;
	private int idade;
	
	// Construtor com informações
	
	public Aluno(int id, String matricula, String nome, int idade) {
		
		this.id = id;
		this.matricula = matricula;
		this.nome = nome;
		this.idade = idade;
		
	}

	// Construtor vazio
	
	public Aluno() {} 
	
	// Getter e Setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}
		

}
