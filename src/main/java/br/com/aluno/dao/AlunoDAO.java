package br.com.aluno.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.aluno.model.Aluno;
import br.com.aluno.util.ConnectionFactory;

/**
 * Classe responsável por trocar informações com o SGBD e fornecer operações CRUD (MySQL)
 * 
 * @author Wellington 02/02/2022
 * @version 1.0
 * @since 1.0
 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/Connection.html
 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/PreparedStatement.html
 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/ResultSet.html
 */

public class AlunoDAO {
	
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;
	
	public AlunoDAO(){}
	
	/**
	 * Método para salvar um aluno na base de dados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @param aluno objeto da classe Aluno
	 * @return status da requisição	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
	 * @return status da requisição	
	 * 
	 */
	
	public boolean save(Aluno aluno) {
		
		boolean status = false; // variável para verificar o status da requisição
		
		String query = "INSERT INTO aluno (id, matricula, nome, idade) VALUES (null, ?, ?, ?)"; // Query SQL
		
		try {
			
			conn = ConnectionFactory.getConnection(); // Estabelecer uma conexão
			ps = conn.prepareStatement(query); // Preparar a query
			
			// Preparar atributos
			
			ps.setString(1, aluno.getMatricula());
			ps.setString(2, aluno.getNome());
			ps.setInt(3, aluno.getIdade());
			
			int rs = ps.executeUpdate(); // Executa a instrução SQL
			
				if(rs > 0) // Se for maior que Zero => executou com sucesso
					status = true;
						
			}catch(SQLException sqle) {
			
				sqle.printStackTrace(); // Imprime o rastreamento do erro da SQL Exception (Console)
			
			}catch(Exception ex) {
			
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
			
			}finally{
			
				ConnectionFactory.closeConnection(conn, ps); // Encerrar conexão ao finalizar		
			
			}
		
		return status;
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para atualizar um aluno da base de dados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @param aluno objeto da classe Aluno
	 * @return status da requisição
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
	 * @return status da requisição	
	 * 
	 */
	
	public boolean update(Aluno aluno) {
		
		boolean status = false; // variável para verificar o status da requisição
		
		String query = "UPDATE aluno SET nome = ?, idade = ? WHERE matricula = ? "; // Query SQL
		
		try {
			
			conn = ConnectionFactory.getConnection(); // Estabelecer uma conexão
			ps = conn.prepareStatement(query); // Preparar a query
					
			// Preparar atributos
			
			ps.setString(1, aluno.getNome());
			ps.setInt(2, aluno.getIdade());
			ps.setString(3, aluno.getMatricula());
			
			int rs = ps.executeUpdate(); // Executa a instrução SQL
			
				if(rs > 0) // Se for maior que Zero => executou com sucesso
					status = true;			
			
			}catch(SQLException sqle) {
			
				sqle.printStackTrace();	// Imprime o rastreamento do erro da SQL Exception (Console)
			
			}catch(Exception ex) {
			
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
			
			}finally{
			
				ConnectionFactory.closeConnection(conn, ps); // Encerrar conexão ao finalizar		
			
			}
		
		return status;
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para excluir um aluno da base de dados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @param matricula número da matricula para exclusão
	 * @return status da requisição	 
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
	 * @return status da requisição	
	 * 
	 */
	
	public boolean delete(String matricula) {
		
		boolean status = false; // variável para verificar o status da requisição
		
		String query = "DELETE FROM aluno WHERE matricula = ? "; // Query SQL
		
		try {
			
			conn = ConnectionFactory.getConnection(); // Estabelecer uma conexão
			ps = conn.prepareStatement(query); // Preparar a query
			
			// Preparar atributos
			
			ps.setString(1, matricula);
			
			int rs = ps.executeUpdate(); // Executa a instrução SQL
			
				if(rs > 0) // Se for maior que Zero => executou com sucesso
					status = true;
			
			}catch(SQLException sqle) {
			
				sqle.printStackTrace(); // Imprime o rastreamento do erro da SQL Exception (Console)
				
			}catch(Exception ex) {
				
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
					
			}finally{
			
				ConnectionFactory.closeConnection(conn, ps); // Encerrar conexão ao finalizar		
			
			}
		
		return status;
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para obter dados de um aluno da base de dados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @param matricula número da matricula para obter dados do aluno
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
	 * @return objeto da classe Aluno	
	 */
	
	public Aluno getAluno(String matricula) {
		
		Aluno aluno = new Aluno(); // Instância o objeto aluno
		
		String query = "SELECT matricula, nome, idade FROM aluno WHERE matricula = ? "; // Query SQL
		
		try {
			
			conn = ConnectionFactory.getConnection(); // Estabelecer uma conexão
			ps = conn.prepareStatement(query); // Preparar a query
						
			// Preparar atributos
			
			ps.setString(1, matricula);
			
			rs = ps.executeQuery(); // Executa a instrução SQL (Consulta)
			
			  // Verifica se exite ao menos um valor
			  // Caso exista dados faz a varredura (While)
				if(rs.isBeforeFirst()) {
					while (rs.next()) {
						
						// Preenche os atributos com informações
						
						aluno.setMatricula(rs.getString(1));
						aluno.setNome(rs.getString(2));
						aluno.setIdade(rs.getInt(3));
											
					}			
				}			
			
			}catch(SQLException sqle) {
			
				sqle.printStackTrace();	// Imprime o rastreamento do erro da SQL Exception (Console)
				
			}catch(Exception ex) {
				
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
					
			}finally{
			
				ConnectionFactory.closeConnection(conn, ps, rs); // Encerrar conexão ao finalizar		
			}
				
		return aluno;		
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para listar todos alunos da base de dados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Exception.html	
	 * @see https://docs.oracle.com/javase/7/docs/api/java/sql/SQLException.html
	 * @return lista com objetos da classe Aluno
	 */
	
	public List<Aluno> listar() {
		
		List<Aluno> lista = new ArrayList<Aluno>(); // Lista para receber objetos do tipo Aluno
		
		String query = "SELECT matricula, nome, idade FROM aluno "; // Query SQL
		
		try {
			
			conn = ConnectionFactory.getConnection(); // Estabelecer uma conexão
			ps = conn.prepareStatement(query); // Preparar a query
						
			rs = ps.executeQuery(); // Executa a instrução SQL (Consulta)
		
			  // Verifica se exite ao menos um valor
			  // Caso exista dados faz a varredura (While)
			
				if(rs.isBeforeFirst()) {
					while (rs.next()) { 
						
						Aluno aluno = new Aluno(); // Instância o objeto aluno
						
						// Preenche os atributos com informações 
						
						aluno.setMatricula(rs.getString(1));
						aluno.setNome(rs.getString(2));
						aluno.setIdade(rs.getInt(3));
						
						lista.add(aluno); // Adiciona o objeto a lista
											
					}			
				}						
			
			}catch(SQLException sqle) {
			
				sqle.printStackTrace();	// Imprime o rastreamento do erro da SQL Exception (Console)
				
			}catch(Exception ex) {
				
				ex.printStackTrace(); // Imprime o rastreamento do erro da Exception (Console)
				
			}finally{
				
				ConnectionFactory.closeConnection(conn, ps, rs); // Encerrar conexão ao finalizar		
				
			}
				
			return lista;
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------	

}
