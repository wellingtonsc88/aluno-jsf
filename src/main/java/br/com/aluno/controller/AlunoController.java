package br.com.aluno.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import br.com.aluno.dao.AlunoDAO;
import br.com.aluno.model.Aluno;

/**
 * Classe de controle que delega funções para as views
 * RequestScoped cada requisição é gerada uma nova instância do Bean (utilizado amplamente em formulários)
 * 
 * @author Wellington 02/02/2022
 * @version 1.0
 * @since 1.0
 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/FacesContext.html
 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html
 */

@ManagedBean(name="alunoController")
@RequestScoped
public class AlunoController {
	
	private Aluno aluno;
	private List<Aluno> lista;
	private String matricula;
	
	FacesContext facesContext;
	ExternalContext externalContext;
				
	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	public List<Aluno> getLista() {
		return lista;
	}
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@PostConstruct // @PostConstruct annotation 
	public void init() { 
		
		// quando o Bean é inicializado podemos utilizar as dependencias nesse método (variáveis, métodos e etc).
		
		facesContext = FacesContext.getCurrentInstance(); // instânciar objeto facesContext
		externalContext = facesContext.getExternalContext(); // instânciar objeto externalContext
		
		externalContext.getSessionMap().put("form", "save"); // Variável da sessão com o valor save (Default) (PreLoaderView)
																	
		listar(); // chamada ao método listar alunos
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para obter informações do usuário ao carregar a página para atualização (called on event preRenderView)
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html
	 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html
	 *
	 */
	
	public void getUpdateInfo() {
							
		if(matricula != null) {
											
			RequestContext.getCurrentInstance().execute("$('#matricula').prop('disabled', true);"); // Desabilitar o campo da matrícula (Atualização)
			
			externalContext.getSessionMap().put("form", "update"); // Variável da sessão "form" com o valor update (PreLoaderView)
				
			AlunoDAO dao = new AlunoDAO(); // instânciar objeto da classe AlunoDAO
					
			aluno = dao.getAluno(matricula); // obter dados do aluno
			
			// Executar JS (manipular alerta)
			// Mostra uma mensagem de erro e / ou sucesso (logo desaparece em X segundos)
			
			if(aluno == null)
			     RequestContext.getCurrentInstance().execute("$('#error').removeClass('d-none').html('Erro ao carregar dados para atualização'); setTimeout(function () {$('#error').addClass('d-none').html('')}, 3000)");
								
		}
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Método para salvar um aluno
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html	 
	 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html
	 *
	 */
	
	public void save() {
		
		boolean status = false;	// Variável para verificar o status da solicitação
		
		Map<String, String> params = externalContext.getRequestParameterMap(); // Recebe nomes de parâmetros incluidos na requisição atual
		
			AlunoDAO dao = new AlunoDAO(); // instânciar objeto da classe AlunoDAO
			Aluno aluno = new Aluno(); // instânciar o objeto da classe Aluno
		
			aluno.setMatricula(params.get("matricula")); 
			aluno.setNome(params.get("nome"));
			aluno.setIdade(Integer.parseInt(params.get("idade")));
		
			status = dao.save(aluno); // salvar aluno
				
			// Executar JS (manipular alerta)
			// Mostra uma mensagem de erro e / ou sucesso (logo desaparece em X segundos)
			
			if(status)
				RequestContext.getCurrentInstance().execute("$('#success').removeClass('d-none').html('Aluno cadastrado com sucesso'); setTimeout(function () {$('#success').addClass('d-none').html('')}, 3000)");
							
			else 
				RequestContext.getCurrentInstance().execute("$('#error').removeClass('d-none').html('Erro ao cadastrar aluno'); setTimeout(function () {$('#error').addClass('d-none').html('')}, 3000)");
					
	}
		
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para atualizar um aluno
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html	
	 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html
	 *
	 */
	
	public void update() {
		
		boolean status = false; // Variável para verificar o status da solicitação
		
		Map<String, String> params = externalContext.getRequestParameterMap(); // Recebe nomes de parâmetros incluidos na requisição atual
				
			AlunoDAO dao = new AlunoDAO(); // instânciar objeto da classe AlunoDAO
			Aluno aluno = new Aluno(); // instânciar o objeto da classe Aluno
							
			aluno.setMatricula(matricula); // Matrícula é uma valor imutável (valor que vem pela URL, instânciado na variável matricula).
			aluno.setNome(params.get("nome")); 
			aluno.setIdade(Integer.parseInt(params.get("idade")));
			
			status = dao.update(aluno); // Atualizar aluno
			
			// Executar JS (manipular alerta)
			// Mostra uma mensagem de erro e / ou sucesso (logo desaparece em X segundos)
			
			if(status)
				RequestContext.getCurrentInstance().execute("$('#success').removeClass('d-none').html('Aluno atualizado com sucesso'); setTimeout(function () {$('#success').addClass('d-none').html('')}, 3000)");
							
			else 
				RequestContext.getCurrentInstance().execute("$('#error').removeClass('d-none').html('Erro ao atualizar aluno'); setTimeout(function () {$('#error').addClass('d-none').html('')}, 3000)");
				
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para exclusão de um aluno
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html
	 */
	
	public void delete() {
		
		boolean status = false;	// Variável para verificar o status da solicitação
		
			AlunoDAO dao = new AlunoDAO();  // instânciar objeto da classe AlunoDAO
							
			status = dao.delete(matricula);  // deletar aluno pela matricula
			
			// Executar JS (manipular alerta)
			// Mostra uma mensagem de erro e / ou sucesso (logo desaparece em X segundos)
			
			if(status) {
				
				removeDelValue(); // Remove o valor da matrícula da sessão
				
				RequestContext.getCurrentInstance().execute("$('#success').removeClass('d-none').html('Deleção de aluno realizada com sucesso'); setTimeout(function () {$('#success').addClass('d-none').html('')}, 3000)");
				
				listar(); // chamada ao método listar aluno
				
			}else 
				RequestContext.getCurrentInstance().execute("$('#error').removeClass('d-none').html('Erro ao executar deleção de aluno'); setTimeout(function () {$('#error').addClass('d-none').html('')}, 3000)");
								
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para obter dados de um aluno
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @return objeto do tipo Aluno
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html
	 *
	 */
	
	public Aluno obterAluno(String matricula) {
		
		Aluno aluno = new Aluno(); // instânciar o objeto da classe Aluno	 
		AlunoDAO dao = new AlunoDAO();	// instânciar objeto da classe AlunoDAO
		
			aluno = dao.getAluno(matricula); // obter dados do aluno
			
			// Executar JS (manipular alerta)
			// Mostra uma mensagem de erro e / ou sucesso (logo desaparece em X segundos)
			
			if(aluno == null)
				RequestContext.getCurrentInstance().execute("$('#error').removeClass('d-none').html('Erro ao encontrar aluno'); setTimeout(function () {$('#error').addClass('d-none').html('')}, 3000)");
			
		return aluno;
					
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para listar alunos registrados
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	
	public void listar(){
		
		AlunoDAO dao = new AlunoDAO(); // instânciar objeto da classe AlunoDAO		
			
		lista = dao.listar();	// listar alunos		
				
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para obter dados de um aluno para view (exclusão)
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0
	 * @see https://www.primefaces.org/docs/api/6.1/index.html?org/primefaces/context/RequestContext.html
	 */
	
	public void getDeleteInfo(){
					
		aluno = obterAluno(matricula);	// Obter dados do aluno pela matrícula
		
			externalContext.getSessionMap().put("deleteValue", matricula); // matricula para exclusão (variável de sessão)
				
			RequestContext.getCurrentInstance().update("delete-form:modal-name"); // Atualizar elemento modal-name
			RequestContext.getCurrentInstance().update("delete-form:modal-matricula"); // Atualizar elemento modal-matricula
					
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método para remover o atributo "deleteValue" da sessão
	 * 
	 * @author Wellington 02/02/2022
	 * @version 1.0
	 * @since 1.0	
	 * @see https://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html
	 */
	
	public void removeDelValue() {
		
			externalContext.getSessionMap().remove("deleteValue"); // Remover variável da sessão
		
	}
	
	// -----------------------------------------------------------------------------------------------------------------------------------

}
