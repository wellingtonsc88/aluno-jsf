/** Arquivo de Validação de formulário
 *  
 * @author Wellington : 2022-02-02
 * @summary Arquivo contendo métodos de apoio para validação 
 * @since version 1.0
 * @version 1.0
 * @see "https://jqueryvalidation.org/" 
 **/

$(function () {
	
	validateName('#nome', '#nome-error'); // Validação do nome

    validateForm('#submit'); // Validação de formulário            

    // Validar formulário ao trocar valor
    validateOnChange('#matricula');
    validateOnChange('#nome');
    validateOnChange('#idade');
        
    // Remover icone de validação
     removeValidationIcon("reset-btn", 'matricula');
    removeValidationIcon("reset-btn", 'nome');
    removeValidationIcon("reset-btn", 'idade');
  
    // Limpar validação do formulário
    resetFormValidation("#submit", "reset-btn");
	
});


// ------------------------------------------------------------------------------------------------------

/** Validação de nome
 *  
 * @author Wellington : 2022-02-02
 * @summary validação de uma nome com letras com / sem acento e espaço em branco
 * @since version 1.0
 * @version 1.0
 * @param {string} id id do campo a ser verificado
 * @param {string} idError id do label de erro
 * @returns {string} mensagem condicional
**/

function validateName(id, idError){

	$.validator.addMethod("isName", function() { 
	   
        let nome = $(id).val();
	    let regex = new RegExp(/^[A-Za-z\u00C0-\u00FF. ]*$/); // REGEX PARA VALIDAÇÃO
	   	    
	    if(!nome.match(regex)){
	    	
		    $(id).removeClass('valid').addClass('invalid');
		    $(idError).parent().find('.error').fadeIn(500);		    
		    return false;
	    	
	    }else $(id).removeClass('invalid').addClass('valid'); return true;
	
	}, "* Nome inválido"); 
}

// ------------------------------------------------------------------------------------------------------

/** Resetar validação
 *  
 * @author Wellington : 2022-02-02
 * @summary Função usada para resetar a validação de um formulário
 * @since version 1.0
 * @version 1.0 
 * @param {string} formId id do formulário a ser verificado
 * @param {string} btn classe do botão reset
 * @returns {void}
**/

function resetFormValidation(formId, btn) {

    // Ação do botão reset ativa a função

	$('.' + btn).click(function () {

		var validator = $(formId).validate();
		validator.resetForm();
		       
		$("input .valid-icon-visible").removeClass("valid-icon-visible").addClass('valid-icon-hidden'); // Trocar classe dos icones (inputs)
	
	})
}

// ------------------------------------------------------------------------------------------------------

/** Validar na mudança de valores
 *  
 * @author Wellington : 2022-02-02
 * @summary Função que verifica a validação quando houver mudança
 * @since version 1.0
 * @version 1.0
 * @param {string} id id do elemento
 * @returns {void}
 * 
**/

function validateOnChange(id) {
	
	$(id).on('change', function () {
		if ($(id).valid())
				$(id).removeClass('invalid');

		else $(id).removeClass('valid');

	})
}

// ------------------------------------------------------------------------------------------------------

/** Remover icone de validação
 *  
 * @author Wellington : 2022-02-02
 * @summary Função usada para remover icone de validação dos inputs
 * @since version 1.0
 * @version 1.0
 * @param {string} btn classe do botão reset
 * @param {string} elem id do elemento
 * @returns {void}
**/

function removeValidationIcon(btn, elem) {

	// Ação do botão reset ativa a função

	$('.' + btn).click(function () {
	
		$('span[for=' + elem + ']').removeClass('valid-icon-visible').addClass('valid-icon-hidden'); // Mudança da classe da elemento
	
			resetFieldValue('#' + elem); // Reseta o valor do campo
	});
}

// ------------------------------------------------------------------------------------------------------

/** Limpar valores dos campos
 *  
 * @author Wellington : 2022-02-02
 * @summary Função para limpar valor dos campos
 * @since version 1.0
 * @version 1.0
 * @param {string} id id do elemento
 * @returns {void}
**/

function resetFieldValue(id) {
	$(id).val('');
}

// ------------------------------------------------------------------------------------------------------

/** Validar formulário
 *
 * @author Wellington : 2022-02-02
 * @summary Função para validar formulário de manipulação de dados do aluno
 * @since version 1.0
 * @version 1.0
 * @param {string} formId id do form a ser verificado
 * @returns {void}
**/

function validateForm(formId){

	$(formId).validate({
		rules:{
			
			matricula:{
				required: true,					
				minlength: 4,
				maxlength: 45,
				number: true
							
			},
			
			nome:{
				required: true,					
				minlength: 4,
				maxlength: 45,
				isName: true
							
			},
			
			idade:{
				required: true,					
				maxlength: 3,
				number: true
						
			}
		},

		messages: {	
				
			 matricula: {
				required: " * Campo obrigatório",
				minlength: "A matricula deve ter pelo menos 4 caracteres",
				maxlength: "A matricula deve ter no máximo 10 caracteres",
				number: " * Campo númerico"
			},
			
			 nome: {
				required: " * Campo obrigatório",
				minlength: "O nome deve ter pelo menos 4 caracteres",
				maxlength: "O nome deve ter no máximo 45 caracteres"
			},
			
			 idade: {
				required: " * Campo obrigatório",			
				maxlength: "A idade deve ter no máximo 3 caracteres",
				number: " * Campo númerico"
			}
			
		  },												
	
		  errorClass: "error",
		  validClass: "success",
		  errorElement: "label",
		
		errorPlacement: function (error, element) {}, // Verificação de erro (Opcional)

		success: function (label, element) {
			
		    // Ao verificar se não tiver erro de entrada			
			$(element.form).find("span[for=" + element.id + "]").removeClass('valid-icon-hidden').addClass('valid-icon-visible'); // Mostar icone de validação (Sucesso)

			//FontAwesome Icon Check for success
			$(element.form).find("span[for=" + element.id + "]").html("<i class='fa fa-check success'></i>"); // Adiciona icone de sucesso									
			
		}, // Verificação de Sucesso (Opcional)
		
		// Destacado
		highlight: function (element, errorClass) {
			
			$(element.form).find("label[for=" + element.id + "]").addClass(errorClass); // Adiciona classe de erro
			$(element.form).find("label[for=" + element.id + "]").removeClass('d-none'); // Remove o estilo display none

			$(element.form).find("input[id=" + element.id + "]").removeClass('valid').addClass('invalid'); // Troca classe de válida para inválida (input element)
						
			$(element.form).find("span[for=" + element.id + "]").removeClass('valid-icon-hidden').addClass('valid-icon-visible'); // Mostra icone de validação
			
			$(element.form).find("span[for=" + element.id + "]").html("<i class='fa fa-times error'></i>"); // Implementa o icone de erro
			
		},
		
		// Sem destaque
		unhighlight: function (element, errorClass) {
			
			$(element.form).find("label[for=" + element.id + "]").removeClass(errorClass); // Adiciona a classe de sucesso
			$(element.form).find("label[for=" + element.id + "]").addClass('d-none'); // Adiciona o estilo display none

			$(element.form).find("input[id=" + element.id + "]").removeClass('invalid').addClass('valid'); // Troca classe de inválida para válida (input element)
						
			$(element.form).find("span[for=" + element.id + "]").html("<i class='fa fa-check success'></i>"); // Implementa o icone de sucesso
			
		}             	                                                         
    });
}

// ---------------------------------------------------------------------------------------------------------------------

