/** Arquivo de Validação de formulário
 *  
 * @author Wellington : 2022-02-02
 * @summary Arquivo contendo métodos de configuração da tabela de alunos
 * @since version 1.0
 * @version 1.0
 * @see "https://datatables.net/"
 **/

$(function () {
	
  // Definições da tabela de alunos

  $('#aluno-table').DataTable({
       	scrollY: '50vh',
        scrollCollapse: true,
        "scrollX": true,
        "scrollCollapse": true,
        "paging": true,
        "bInfo": false,
        "deferRender": true,
        "ordering": true        
   });

	 // Prevenir o recarregamento de ações ja enviadas pelo formulário
	   window.history.replaceState('','',window.location.href)	

});