# Projeto Aluno

O projeto foi desenvolvido em JSF 2.2 (Java Server Faces) utilizando MySQL Server como SGDB.

A aplicação possui integração com JQuery Plugins (Como JQuery Validate e JQuery Datatable), Bootstrap v4.4.1 e FontAwesome v5.13.0.

A aplicação consiste em listar, cadastrar, atualizar e excluir alunos do sistema, possuindo uma finalidade didática para quem está 
iniciando no framewrok JSF.
